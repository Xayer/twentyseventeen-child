### twentyseventeen-child

This is a child theme for twentyseventeen that adds custom shortcodes,
that makes it easy to build the perfect website in a simple and easy way.

### shortcodes examples
Add the child theme and go to Appearance > shortcodes and see what shortcodes you can use.

I will be adding more cool stuff to the child theme soon :)