<?php
function helloworld_shortcode($atts){
	return "Hello World!";
}
add_shortcode( 'helloworld', 'helloworld_shortcode' );

/* DIV */
function div_class_shortcode($atts, $content="") {
    extract(shortcode_atts(array(
        'class' => '',
        'id' => '',
    ), $atts));
	
    if($id)
        $div .= " id='{$id}'";
    if($class) 
        $div .= " class='{$class}'";
    elseif(!$id)
        $div = " class='{$class}'";
    return "<div $div>" . do_shortcode($content) . "</div>";
}
add_shortcode('div', 'div_class_shortcode');
/* DIV */

/* SPAN */
function span_class_shortcode($atts, $content="") {
    extract(shortcode_atts(array(
        'class' => '',
        'id' => '',
    ), $atts));
    
    if($id)
        $span .= " id='{$id}'";
    if($class) 
        $span .= " class='{$class}'";
    elseif(!$id)
        $span = " class='{$class}'";
    return "<span $span>" . do_shortcode($content) . "</span>";
}
add_shortcode('span', 'span_class_shortcode');
/* SPAN */

/* GRID */
function grid_shortcode( $atts, $content = "", $tag ){
    $tag = explode( '_', $tag );
    $tag = $tag[1]; 

    extract(shortcode_atts(array(
        'class' => '',
        'id' => ''
    ), $atts));

    $div = "";

    if( empty($class) )
        $class = "grid-cols-$tag";
    else
        $class = "$class grid-cols-$tag";

    if( $tag == 12 )
        $class = "$class clear";

    if( $id )
        $div .= " id='{$id}'";
    
    $div .= " class='{$class}'";
    return "<div $div>" . do_shortcode($content) . "</div>";
}
$grid_amount = array('grid_1','grid_2','grid_3','grid_4','grid_5','grid_6','grid_7','grid_8','grid_9','grid_10','grid_11','grid_12');

foreach( $grid_amount as $grid ){
    add_shortcode( $grid, 'grid_shortcode' );
}
/* GRID */