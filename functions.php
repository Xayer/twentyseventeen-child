<?php
require_once('shortcodes.php');

add_action( 'admin_menu', 'shortcodes_menu' );
function shortcodes_menu() {
	add_theme_page('Shortcodes', 'Shortcodes', 'manage_options', 'shortcodes', 'shortcodes_options' );
}

function shortcodes_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<h1>Shortcodes</h1>';
	echo '<hr/>';
	echo '<p>The [div] Shortcode:</p>';
	echo '<p><b>[div]</b><br/>This is a div without a class or id.</br><b>[/div]</b></p>';
	echo '<p><b>[div class="test"]</b><br/>This is a div with a class.</br><b>[/div]</b></p>';
	echo '<p><b>[div id="test"]</b><br/>This is a div with a id.</br><b>[/div]</b></p>';
	echo '<hr/>';
	echo '<p>The [span] Shortcode:</p>';
	echo '<p><b>[span]</b><br/>This is a span without a class or id.</br><b>[/span]</b></p>';
	echo '<p><b>[span class="test"]</b><br/>This is a span with a class.</br><b>[/span]</b></p>';
	echo '<p><b>[span id="test"]</b><br/>This is a span with a id.</br><b>[/span]</b></p>';
	echo '<hr/>';
	echo '<p>The [grid] Shortcode:</p>';
	echo '<p><b>[grid_1]</b><br/>This is grid_1 with the width of 6.5%.<br/><b>[/grid_1]</b></p>';
	echo '<p><b>[grid_2]</b><br/>This is grid_2 with the width of 15%.<br/><b>[/grid_2]</b></p>';
	echo '<p><b>[grid_3]</b><br/>This is grid_3 with the width of 23.5%.<br/><b>[/grid_3]</b></p>';
	echo '<p><b>[grid_4]</b><br/>This is grid_4 with the width of 32%.<br/><b>[/grid_4]</b></p>';
	echo '<p><b>[grid_5]</b><br/>This is grid_5 with the width of 40.5%.<br/><b>[/grid_5]</b></p>';
	echo '<p><b>[grid_6]</b><br/>This is grid_6 with the width of 49%.<br/><b>[/grid_6]</b></p>';
	echo '<p><b>[grid_7]</b><br/>This is grid_7 with the width of 57.5%.<br/><b>[/grid_7]</b></p>';
	echo '<p><b>[grid_8]</b><br/>This is grid_8 with the width of 66%.<br/><b>[/grid_8]</b></p>';
	echo '<p><b>[grid_9]</b><br/>This is grid_9 with the width of 74.5%.<br/><b>[/grid_9]</b></p>';
	echo '<p><b>[grid_10]</b><br/>This is grid_10 with the width of 83%.<br/><b>[/grid_10]</b></p>';
	echo '<p><b>[grid_11]</b><br/>This is grid_11 with the width of 91.5%.<br/><b>[/grid_11]</b></p>';
	echo '<p><b>[grid_12]</b><br/>This is grid_12 with the width of 100%.<br/><b>[/grid_12]</b></p>';
	echo '</div>';
}